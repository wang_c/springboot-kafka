package com.example.kafkaconsumer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * @author wangc
 * @date 2020.7.3
 */
@Component
public class KafkaMessageConsumerService {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaMessageConsumerService.class);

    private static final String group1 = "wc1";

    private static final String group2 = "wc2";

    @KafkaListener(topics={"${kafka.app.topic.foo}"},groupId = group1)
    public void receive11(@Payload String message){
        LOG.info("KafkaMessageConsumer1-1 接收到消息："+message);
    }

    @KafkaListener(topics = {"${kafka.app.topic.foo}"},groupId = group1)
    public void receive12(@Payload String message){
        LOG.info("KafkaMessageConsumer1-2 接收到消息："+message);
    }

    @KafkaListener(topics = {"${kafka.app.topic.foo}"},groupId = group2)
    public void receive21(@Payload String message){
        LOG.info("KafkaMessageConsumer2-1 接收到消息："+message);
    }

    @KafkaListener(topics = {"${kafka.app.topic.foo}"},groupId = group2)
    public void receive22(@Payload String message){
        LOG.info("KafkaMessageConsumer2-2 接收到消息："+message);
    }
}
